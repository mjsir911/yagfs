use std::env;
use fuse_mt::*;
use std::ffi::{OsStr};
use stderrlog;
use log::{debug,info};
use std::path::{Path,PathBuf};
use time::{Timespec};
use git2;
use git2::{Repository,BranchType,TreeEntry,ObjectType,Object,Oid};
use std::sync::{Mutex};

struct YAGFS {
	pub repo: Mutex<Repository>,
}

fn git2fuse(t: ObjectType) -> FileType {
	match t {
		ObjectType::Blob => FileType::RegularFile,
		ObjectType::Tree => FileType::Directory,
		_ => unreachable!()
	}
}

enum FilesystemThing<'r> {
	File(git2::TreeEntry<'r>),
	Directory(git2::TreeEntry<'r>),
	Root(git2::Tree<'r>),
}

// impl<'r> FilesystemThing<'r> {
// 	fn tree(&self, repo: &'r Repository) -> &git2::Tree<'r> {
// 		match self {
// 			FilesystemThing::Directory(entry) => entry.to_object(repo).unwrap().as_tree().unwrap(),
// 			FilesystemThing::Root(tree) => tree,
// 			FilesystemThing::File(_) => unreachable!(),
// 		}
// 	}
// }

struct GitFile<'r> {
	repo: &'r Repository,
	commit: git2::Commit<'r>,
	thing: FilesystemThing<'r>,
}

impl<'r> FilesystemThing<'r> {
	fn kind(&self) -> FileType {
		match self {
			FilesystemThing::File(_) => FileType::RegularFile,
			FilesystemThing::Root(_) | FilesystemThing::Directory(_) => FileType::Directory,
		}
	}
}

impl<'r> GitFile<'r> {
	fn new(repo: &'r Repository, path: &Path) -> GitFile<'r> {
		debug!("GitFile::new({:?})", path);
		let branchname: &str = path.iter().nth(1).unwrap().to_str().unwrap().into();
		let branch = repo.find_branch(branchname, BranchType::Local).unwrap();
		let commit = branch.into_reference().peel_to_commit().unwrap();
		let path = path.strip_prefix(format!("/{}", branchname)).unwrap();
		let tree = commit.tree().unwrap();

		let thing = if path.to_str().unwrap().is_empty() {
			FilesystemThing::Root(tree)
		} else {
			let thing = tree.get_path(path).unwrap();
			match thing.kind().unwrap() {
				ObjectType::Blob => FilesystemThing::File(thing),
				ObjectType::Tree => FilesystemThing::Directory(thing),
				_ => unreachable!()
			}
		};

		GitFile {repo: repo, commit: commit, thing: thing}
	}

	fn getattr(&self) -> (Timespec, FileAttr) {
		let time = Timespec{sec: self.commit.time().seconds(), nsec: 0};
		let size = match &self.thing {
			FilesystemThing::Directory(_) => 0,
			FilesystemThing::Root(_) => 0,
			FilesystemThing::File(entry) => entry.to_object(self.repo).unwrap().into_blob().unwrap().size() as u64,
		};
		let kind = self.thing.kind();
		let perm = match &self.thing {
			FilesystemThing::Directory(entry) => entry.filemode(),
			FilesystemThing::File(entry) => entry.filemode(),
			FilesystemThing::Root(_) => 0x777,
		} as u16;
		(time, FileAttr {
			size: size, blocks: 0, kind: kind, perm: perm,
			atime: time, mtime: time, ctime: time, crtime: time,
			uid: 1000, gid: 1000, nlink: 1, rdev: 0, flags: 0,
		})
	}

	fn readdir(&self) -> Vec<DirectoryEntry> {
		match &self.thing {
			FilesystemThing::Directory(entry) => {
				entry.to_object(self.repo).unwrap().peel_to_tree().unwrap().iter()
					.map(|t| DirectoryEntry{
						name: t.name().unwrap().into(),
						kind: git2fuse(t.kind().unwrap()) 
					})
					.collect::<Vec<DirectoryEntry>>()
			}
			FilesystemThing::File(_) => unreachable!(),
			FilesystemThing::Root(tree) => {
				tree.iter()
					.map(|t| DirectoryEntry{
						name: t.name().unwrap().into(),
						kind: git2fuse(t.kind().unwrap()) 
					})
					.collect::<Vec<DirectoryEntry>>()
			}
		}
	}

	fn read(&self, _offset: u64, _size: u32, result: impl FnOnce(Result<&[u8], i32>)) {
		match &self.thing {
			FilesystemThing::Directory(_) | FilesystemThing::Root(_) => result(Err(1)),
			FilesystemThing::File(entry) => result(Ok(entry.to_object(self.repo).unwrap().peel_to_blob().unwrap().content()))
		};
	}
}

impl FilesystemMT for YAGFS {
	fn init(&self, _req: RequestInfo) -> ResultEmpty {
		debug!("init");
		Ok(())
	}

	fn getattr(&self, _req: RequestInfo, path: &Path, fh: Option<u64>) -> ResultEntry {
		// r.find_branch(_path.file_name().unwrap().to_str().unwrap(), BranchType::Local).unwrap().into_reference().peel_to_tree().unwrap().iter()
		if path == Path::new("/") {
			let zerotime = Timespec { sec: 0, nsec: 0 };
			return Ok((zerotime, FileAttr {size: 0, blocks: 0, atime: zerotime, mtime: zerotime, ctime: zerotime, crtime: zerotime, kind: FileType::Directory, perm: 0x755, uid: 1000, gid: 1000, rdev: 0, flags: 0, nlink: 1}));

		}
		let r = self.repo.lock().unwrap();
		let file = GitFile::new(&r, path);
		return Ok(file.getattr());
	}

	fn utimens(&self, _req: RequestInfo, _path: &Path, _fh: Option<u64>, _atime: Option<Timespec>, _mtime: Option<Timespec>) -> ResultEmpty {
		debug!("utimens");
		Ok(()) 
	}

    fn readlink(&self, _req: RequestInfo, _path: &Path) -> ResultData {
		debug!("readlink");
		Err(1) 
	}

    fn unlink(
        &self,
        _req: RequestInfo,
        _parent: &Path,
        _name: &OsStr
    ) -> ResultEmpty { Ok(()) }
    fn link(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _newparent: &Path,
        _newname: &OsStr
    ) -> ResultEntry { Err(1) }

	fn open(&self, _req: RequestInfo, _path: &Path, _flags: u32) -> ResultOpen { 
		debug!("open");
		Ok((0, 0))
	}

	fn read(&self, _req: RequestInfo, _path: &Path, _fh: u64, _offset: u64, _size: u32, result: impl FnOnce(Result<&[u8], i32>)) {
		let r = self.repo.lock().unwrap();
		return GitFile::new(&r, _path).read(_offset, _size, result);
	}

    fn release(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _fh: u64,
        _flags: u32,
        _lock_owner: u64,
        _flush: bool
    ) -> ResultEmpty { Ok(()) }
    fn fsync(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _fh: u64,
        _datasync: bool
    ) -> ResultEmpty { Ok(()) }
    fn opendir(&self, _req: RequestInfo, _path: &Path, _flags: u32) -> ResultOpen { 
		debug!("opendir(path: {:?}, flags: {:?})", _path, _flags);
		if _path == Path::new("/") { 
			Ok((0, 1)) 
		} else {
			Ok((1, 0))
		}
	}

    fn readdir(&self, _req: RequestInfo, _path: &Path, _fh: u64) -> ResultReaddir { 
		debug!("readdir(_path: {:?}, _fh: {:?})", _path, _fh);
		let r = self.repo.lock().unwrap();
		let files: Vec<DirectoryEntry> = if _fh == 0 {
			// root
			r.branches(None).unwrap()
				.map(|b| b.unwrap().0.name().unwrap().unwrap().to_owned())
				.filter(|bn| ! bn.contains("/")) // git branches with / are a no-no
				.map(|bn| DirectoryEntry{name: bn.into(), kind:FileType::Directory})
				.collect::<Vec<DirectoryEntry>>()
		} else {
			GitFile::new(&r, _path).readdir()
		};
		debug!("readdir -> {:?}", files);
		Ok(files)
	}
    fn releasedir(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _fh: u64,
        _flags: u32
    ) -> ResultEmpty { Ok(()) }
    fn fsyncdir(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _fh: u64,
        _datasync: bool
    ) -> ResultEmpty { Ok(()) }
    fn statfs(&self, _req: RequestInfo, _path: &Path) -> ResultStatfs { Err(1) }
    fn setxattr(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _name: &OsStr,
        _value: &[u8],
        _flags: u32,
        _position: u32
    ) -> ResultEmpty { Ok(()) }
    fn getxattr(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _name: &OsStr,
        _size: u32
    ) -> ResultXattr { Err(1) }
    fn listxattr(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _size: u32
    ) -> ResultXattr { Err(1) }
    fn removexattr(
        &self,
        _req: RequestInfo,
        _path: &Path,
        _name: &OsStr
    ) -> ResultEmpty { Ok(()) }
    fn access(&self, _req: RequestInfo, _path: &Path, _mask: u32) -> ResultEmpty { Ok(()) }
    fn create(
        &self,
        _req: RequestInfo,
        _parent: &Path,
        _name: &OsStr,
        _mode: u32,
        _flags: u32
    ) -> ResultCreate { Err(1) }
}

fn main() {
	let args: Vec<String> = env::args().collect();
	stderrlog::new().verbosity(5).quiet(false).init().unwrap();
    let mountpoint = &args[1];
    let options = ["-o", "ro", "-o", "fsname=hello"]
        .iter()
        .map(|o| o.as_ref())
        .collect::<Vec<&OsStr>>();
	let repo = Mutex::new(Repository::open("/tmp/tmp.E7eBQZjRE4/fuse-mt.git").unwrap());
	fuse_mt::mount(fuse_mt::FuseMT::new(YAGFS {repo: repo}, 1), &mountpoint, &options).unwrap();
}
